import java.awt.EventQueue;
import javax.swing.JFrame;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Main {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() throws Exception {
		initialize();
		Document document;
		String webPage = "https://www.spanishdict.com/traductor/casa";
		document = Jsoup.connect(webPage).get();
		Elements palabra = document.getElementById("quickdef1-es").getElementsByClass("_1btShz4h"); // F12 y se busca el div de la definicion
		System.out.println("PALABRA" + palabra.get(0).html());
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
