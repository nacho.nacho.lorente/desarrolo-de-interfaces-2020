package Traductorclases ;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class ActionButton implements ActionListener{
	
	JTextField textIn = new JTextField();
	JTextField textOut = new JTextField();
		
	public  ActionButton(JTextField textIn, JTextField textOut) {

		this.textIn = textIn;
		this.textOut = textOut;
				

		}

		public void actionPerformed(ActionEvent e) {
		
			if(textIn.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null,
						"Palabra no introducida",
						"Error",
						JOptionPane.ERROR_MESSAGE);
				
			}else {
			
			traducir(textIn.getText(), textOut);
			
			}
			
	}

		private void traducir(String text, JTextField textOut) {

			Document document = null;
			String webPage = "https://www.spanishdict.com/traductor/" + text;
			try {
				document = Jsoup.connect(webPage).get();
			} catch (IOException e) {
				e.printStackTrace();
			}
			Elements palabra = document.getElementById("quickdef1-es").getElementsByClass("_1btShz4h");
			textOut.setText(palabra.get(0).html());
			
		}

}
