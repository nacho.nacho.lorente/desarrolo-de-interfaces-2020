package Traductorclases;

import java.awt.Color;

import javax.swing.*;

public class PanelPersonal extends JPanel {
	
	JLabel palabraIn, palabraOut;
	JTextField textIn, textOut;
	JButton traducir;
	
	PanelPersonal(){
		
		setBackground(Color.LIGHT_GRAY);
		
		setLayout(null);
		
		palabraIn = new JLabel();
		palabraIn.setBounds(80,50,50,50);
		palabraIn.setText("Palabra a traducir");
		//coge el tama�o de palabraIn y le cambia el tama�o
		palabraIn.setSize(palabraIn.getPreferredSize());
		
		add(palabraIn);
		
		palabraOut = new JLabel();
		palabraOut.setBounds(82,150,50,50);
		palabraOut.setText("Palabra traducida");
		//coge el tama�o de palabraIn y le cambia el tama�o
		palabraOut.setSize(palabraOut.getPreferredSize());
		
		add(palabraOut);
		
		textIn = new JTextField();
		textIn.setBounds(200,50,80,20);
		
		add(textIn);
		
		textOut = new JTextField();
		textOut.setBounds(200,150,80,20);
		textOut.setEditable(false);
		
		add(textOut);
		
		traducir = new JButton("Traducir");
		traducir.setBounds(170, 90, 80, 40);
		traducir.setSize(traducir.getPreferredSize());
		traducir.addActionListener(new ActionButton(textIn, textOut));
		
		add(traducir);
		
		
	/*	ActionButton mievento = new ActionButton(textIn, textOut);
	  traducir.addActionListener(mievento);
	*/
	
		traducir.addActionListener(new ActionButton(textIn, textOut));
	}

}
