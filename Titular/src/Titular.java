import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JPanel;

public class Titular {

	private JFrame frmTitular;
	private JTextField textFieldTitular;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Titular window = new Titular();
					window.frmTitular.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Titular() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTitular = new JFrame();
		frmTitular.setTitle("TITULAR");
		frmTitular.setBounds(100, 100, 776, 433);
		frmTitular.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTitular.getContentPane().setLayout(null);
		frmTitular.setLocationRelativeTo(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 587, 255);
		frmTitular.getContentPane().add(panel);
		
		textFieldTitular = new JTextField();
		panel.add(textFieldTitular);
		textFieldTitular.setEditable(false);
		textFieldTitular.setColumns(10);
		
		JButton btnNewButton = new JButton("LANZAR NOTICIA");
		panel.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lanzarNoticia();
			}
		});
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 0, 639, 299);
		frmTitular.getContentPane().add(panel_1);
		
		JButton btnNewButton_1 = new JButton("New button");
		panel_1.add(btnNewButton_1);
	}

	protected void lanzarNoticia() {
		Document document = null;
		String webPage = "https://elpais.com";
		try {
			document = Jsoup.connect(webPage).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Elements palabra = document.getElementsByClass("c_h headline | color_gray_ultra_dark font_secondary width_full  headline_md ");
		textFieldTitular.setText(palabra.get(0).html());
	}
}
