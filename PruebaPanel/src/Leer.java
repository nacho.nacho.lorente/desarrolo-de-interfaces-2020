import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


public class Leer {
	

	public String leer(String archivo)  {
        String cadena = "", 
        		texto = "";
        BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(archivo), "utf-8"));
			
			 while((cadena = br.readLine())!=null) {
		        	texto += cadena;
		        }
		        br.close();
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"El archivo no existe",
					"ERROR",
					JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
		}
                        
        return texto;
      
    }
	
	public void Login (JTextField textFieldUsuario, JPasswordField passwdField, JPanel panelNexth, JPanel panelNextp) {
		File fichero = new File ("src//login.txt");
		boolean login = false;
		String passwd= String.valueOf(passwdField.getPassword());
		String usuario = textFieldUsuario.getText();
		
		try {
			login = encuentra(usuario, passwd, fichero, login);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} /*catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		if (login) {
			panelNextp.setVisible(true);
			panelNexth.setVisible(true);	
		}
		else
		{
			JOptionPane.showMessageDialog(null, "El nombre o la contraseņa son incorrectas", "Error de login", JOptionPane.ERROR_MESSAGE);
		}
		if (fichero.exists() != true) 
		{
			JOptionPane.showMessageDialog(null, "No existe fichero", "Va a ser que no", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
public boolean encuentra(String usuario, String passwd, File fichero, boolean login) throws FileNotFoundException {
    	
        String linea="";
        try {
        BufferedReader br=new BufferedReader(new FileReader(fichero));

        while ((linea= br.readLine())!=null) {  	
        	if(linea.contentEquals(usuario + " " + passwd))
        		login = true;	
        	else if (!login)
                login = false;
         }
         br.close();
        } catch (IOException e) {
            System.out.println("Error en el archivo");
        }   
        
        return login;
    }
	
	
}
