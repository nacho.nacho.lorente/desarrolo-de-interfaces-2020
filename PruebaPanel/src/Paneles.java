

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JSplitPane;
import java.awt.Cursor;
import javax.swing.JTextArea;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;

public class Paneles {

	private JFrame frame;
	JPanel panel = new JPanel();
	JPanel panel_1 = new JPanel();
	JPanel panel_2 = new JPanel();
	JPanel panel_3 = new JPanel();
	JPanel panel_padre = new JPanel();
	JPanel panel_5 = new JPanel();
	JPanel panel_6 = new JPanel();
	JPanel panel_7 = new JPanel();
	JPanel panel_8 = new JPanel();
	File fichero = new File ("texto.txt");
	File ficheroLogin = new File ("login.txt");
	Leer miLectura = new Leer();
	Timer timer;
	Timer timer_2;
	int i = 0;
	int cnt = 0;
	
	private JTextField textField;
	private JTextField textfieldUsuario;
	private JPasswordField passwordField;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Paneles window = new Paneles();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Paneles() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws FileNotFoundException 
	 */
	
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 788, 539);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu unMenu = new JMenu("Help");
		menuBar.add(unMenu);
		
		JMenuItem infoMenu = new JMenuItem("Ayuda");
		infoMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null,
						"Cambia de ventana con botones",
						"INFO",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		unMenu.add(infoMenu);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		
		
		Image IMG = new ImageIcon("src\\paisaje.jpg").getImage();
		
		
		panel.setBackground(Color.CYAN);
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		JLabel lbl0 = new JLabel("Panel 0");
		lbl0.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lbl0, BorderLayout.NORTH);
		
		JButton btn1 = new JButton("Al panel 1");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setVisible(false);
				panel_1.setVisible(true);
				frame.dispose();
				frame.setUndecorated(true);
				frame.setVisible(true);
			}
		});
		btn1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		btn1.setBackground(Color.WHITE);
		btn1.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btn1, BorderLayout.SOUTH);
		
		
		panel_1.setBackground(Color.YELLOW);
		frame.getContentPane().add(panel_1, BorderLayout.SOUTH);
		
		JButton btn11 = new JButton("Al panel 1");
		btn11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setVisible(true);
				panel_1.setVisible(false);
				frame.dispose();
				frame.setUndecorated(false);
				frame.setVisible(true);
				//frame.pack();
			}
		});
		panel_1.add(btn11);
		
		JLabel lbl1 = new JLabel("Panel 1");
		lbl1.setFont(new Font("SansSerif", Font.PLAIN, 20));
		panel_1.add(lbl1);
		
		JButton btn12 = new JButton("Al panel 2");
		btn12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_1.setVisible(false);
				panel_2.setVisible(true);
				frame.dispose();
				frame.setUndecorated(false);
				frame.setVisible(true);
			}
		});
		panel_1.add(btn12);
		
		
		panel_2.setFont(new Font("SansSerif", Font.PLAIN, 25));
		panel_2.setBackground(Color.MAGENTA);
		frame.getContentPane().add(panel_2, BorderLayout.WEST);
		panel_2.setLayout(new GridLayout(0, 1, 0, 0));
		
		JButton btn21 = new JButton("Al panel 1");
		btn21.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_1.setVisible(true);
				panel_2.setVisible(false);
				frame.dispose();
				frame.setUndecorated(true);
				frame.setVisible(true);
			}
		});
		panel_2.add(btn21);
		
		JLabel lbl2 = new JLabel("Panel 2");
		lbl2.setFont(new Font("SansSerif", Font.PLAIN, 20));
		lbl2.setHorizontalTextPosition(SwingConstants.CENTER);
		lbl2.setHorizontalAlignment(SwingConstants.CENTER);
		panel_2.add(lbl2);
		
		JButton btn22 = new JButton("Al panel 3");
		btn22.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_2.setVisible(false);
				panel_3.setVisible(true);
			}
		});
		panel_2.add(btn22);
		
		
		panel_3.setBackground(Color.ORANGE);
		frame.getContentPane().add(panel_3, null);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[] {70, 70, 0, 0, 70, 70};
		gbl_panel_3.rowHeights = new int[] {16, 0, 0, 16, 16, 16};
		gbl_panel_3.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_3.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_3.setLayout(gbl_panel_3);
		
		JLabel lbl_3 = new JLabel("Panel 3");
		lbl_3.setFont(new Font("SansSerif", Font.PLAIN, 20));
		GridBagConstraints gbc_lbl_3 = new GridBagConstraints();
		gbc_lbl_3.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_3.anchor = GridBagConstraints.NORTH;
		gbc_lbl_3.gridx = 3;
		gbc_lbl_3.gridy = 2;
		panel_3.add(lbl_3, gbc_lbl_3);
		
		JButton btn31 = new JButton("Al panel 2");
		btn31.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_3.setVisible(false);
				panel_2.setVisible(true);
			}
		});
		
		JButton bt_panel_4 = new JButton("Al panel 4");
		bt_panel_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_3.setVisible(false);
				panel_padre.setVisible(true);
				panel_5.setVisible(true);
			}
		});
		GridBagConstraints gbc_bt_panel_4 = new GridBagConstraints();
		gbc_bt_panel_4.insets = new Insets(0, 0, 5, 5);
		gbc_bt_panel_4.gridx = 1;
		gbc_bt_panel_4.gridy = 3;
		panel_3.add(bt_panel_4, gbc_bt_panel_4);
		GridBagConstraints gbc_btn31 = new GridBagConstraints();
		gbc_btn31.insets = new Insets(0, 0, 0, 5);
		gbc_btn31.gridx = 1;
		gbc_btn31.gridy = 5;
		panel_3.add(btn31, gbc_btn31);
		
		JButton btnExit = new JButton("SALIR");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				System.exit(0);
			}
		});
		GridBagConstraints gbc_btnExit = new GridBagConstraints();
		gbc_btnExit.insets = new Insets(0, 0, 0, 5);
		gbc_btnExit.gridx = 4;
		gbc_btnExit.gridy = 5;
		panel_3.add(btnExit, gbc_btnExit);
		panel_padre.setBackground(new Color(153, 0, 255));
		
		
		frame.getContentPane().add(panel_padre, null);
		panel_padre.setLayout(new GridLayout(2, 2, 2, 2));
		panel_5.setBackground(Color.RED);
		
		
		panel_padre.add(panel_5);
		panel_5.setLayout(null);
		
		JButton btnAlpanel_5 = new JButton("Al Azul");
		btnAlpanel_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				miLectura.Login(textfieldUsuario, passwordField, panel_padre, panel_6);
				timer_2.start();
			}
		});
		btnAlpanel_5.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		btnAlpanel_5.setBounds(124, 209, 89, 23);
		panel_5.add(btnAlpanel_5);
		
		JLabel lblNewLabel = new JLabel("Usuario:");
		lblNewLabel.setBounds(64, 38, 69, 14);
		panel_5.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Contrase\u00F1a:");
		lblNewLabel_1.setBounds(64, 92, 93, 14);
		panel_5.add(lblNewLabel_1);
		
		textfieldUsuario = new JTextField();
		textfieldUsuario.setBounds(167, 35, 86, 20);
		panel_5.add(textfieldUsuario);
		textfieldUsuario.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(167, 89, 86, 20);
		panel_5.add(passwordField);
		panel_6.setBackground(Color.BLUE);
		
		
		panel_6.setVisible(false);
		panel_padre.add(panel_6);
		panel_6.setLayout(null);
		
		JButton btnAlpanel_5_1 = new JButton("Al Verde");
		btnAlpanel_5_1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				/*panel_padre.setVisible(true);
				panel_7.setVisible(true);
				textField.setText(miLectura.leer("src//texto.txt"));*/
				timer.start();
				
			}
		});
		btnAlpanel_5_1.setBounds(159, 209, 89, 23);
		panel_6.add(btnAlpanel_5_1);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setBounds(135, 134, 146, 14);
		panel_6.add(progressBar);
		panel_7.setBackground(Color.GREEN);
		
		
		panel_7.setVisible(false);
		panel_7.setEnabled(false);
		panel_padre.add(panel_7);
		panel_7.setLayout(null);
		
		
		JButton btnAlpanel_5_2 = new JButton("Al Naranja");
		btnAlpanel_5_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				/*panel_padre.setVisible(true);
				panel_8.setVisible(true);*/
				timer.start();
			}
		});
		
		btnAlpanel_5_2.setBounds(130, 209, 110, 23);
		panel_7.add(btnAlpanel_5_2);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(47, 65, 281, 20);
		panel_7.add(textField);
		textField.setColumns(10);
		
		JProgressBar progressBar_1 = new JProgressBar();
		progressBar_1.setStringPainted(true);
		progressBar_1.setBounds(94, 131, 146, 14);
		panel_7.add(progressBar_1);
		panel_8.setBackground(Color.ORANGE);
		
			
		
		panel_8.setVisible(false);
		panel_8.setEnabled(false);
		panel_padre.add(panel_8);
		panel_8.setLayout(null);
		
		JButton btnNewButton = new JButton("SALIR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnNewButton.setBounds(165, 120, 89, 23);
		panel_8.add(btnNewButton);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon(IMG.getScaledInstance( 390, 243, Image.SCALE_SMOOTH)));
		lblNewLabel_2.setBounds(0, 0, 390, 243);
		panel_8.add(lblNewLabel_2);
		/* No redimensionar*/
	
	timer = new Timer(100, new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			i++;
			progressBar_1.setValue(i + 1);
			if(i == 100) {
				timer.stop();
				
			panel_padre.setVisible(true);
			panel_8.setVisible(true);
			
		}
	
	
		}
	});
	
	timer_2 = new Timer(100, new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			cnt++;
			progressBar.setValue(cnt + 1);
			if(cnt == 100) {
				timer.stop();
			//panel_padre.setVisible(true);
			panel_7.setVisible(true);
			panel_padre.setVisible(true);
			textField.setText(miLectura.leer("src//texto.txt"));
		}
	
	
		}
	});
	
	}
}