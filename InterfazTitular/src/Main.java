import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Image;
import java.awt.TextArea;
import java.awt.Scrollbar;

public class Main {

	private JFrame frmMain;
	// TextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmMain.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMain = new JFrame();
		frmMain.setResizable(false);
		frmMain.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\a.jpg"));
		frmMain.setTitle("TRADUCTOR NACHO");
		frmMain.setBounds(100, 100, 579, 385);
		frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMain.getContentPane().setLayout(null);
		frmMain.setLocationRelativeTo(null);

		JLabel lblResultado = new JLabel("Resultado:");
		lblResultado.setHorizontalAlignment(SwingConstants.CENTER);
		lblResultado.setBounds(10, 183, 74, 14);
		frmMain.getContentPane().add(lblResultado);

		TextArea textArea = new TextArea("", 0, 0, TextArea.SCROLLBARS_HORIZONTAL_ONLY);
		textArea.setBounds(109, 183, 380, 71);
		frmMain.getContentPane().add(textArea);

		JButton btnTraducir = new JButton("TITULAR");
		btnTraducir.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		btnTraducir.setIcon(null);
		btnTraducir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				titular(textArea);

			}
		});
		btnTraducir.setBounds(209, 80, 115, 23);
		frmMain.getContentPane().add(btnTraducir);

	}

	public void titular(TextArea area) {

		Document document = null;
		String webPage = "https://www.marca.com/tiramillas/actualidad/2020/10/27/5f979ad746163fb76f8b45c5.html";
		try {
			document = Jsoup.connect(webPage).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Elements palabra = document.getElementsByClass("js-headline izquierda");
		area.setText(palabra.get(0).html());
	}
}
